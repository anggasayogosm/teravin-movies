import {FlatList, SafeAreaView, Text, View, Image} from 'react-native';
import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {MoviesActions} from '../../Store/Redux/MovieReducer';
import {styles} from './styles';
import {IMG} from '../../Services/ApiServices';

const HomeScreen = props => {
  const {doGetMovies, movies} = props;

  useEffect(() => {
    doGetMovies();
  }, []);

  return (
    <SafeAreaView style={styles.contain}>
      <Text style={styles.brand}>Teravin Movies</Text>
      <View style={{flex: 1}}>
        <FlatList
          data={movies.results}
          keyExtractor={(_, i) => i.toString()}
          renderItem={({item}) => (
            <View style={styles.contain}>
              <Text style={styles.title}>{item.title}</Text>
              <Image
                source={{uri: `${IMG.path}${item.poster_path}`}}
                style={styles.mvimages}
                resizeMode="cover"
              />
              <Text style={styles.desc}>{item.overview}</Text>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = state => ({
  movies: state.MoviesReducer.movie.data,
});

const mapDispatchToProps = dispatch => ({
  doGetMovies: () => dispatch(MoviesActions.doGetMovieListRequest()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
