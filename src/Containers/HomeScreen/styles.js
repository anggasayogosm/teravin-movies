import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  mvimages: {
    width: '100%',
    height: 300,
  },
  contain: {
    backgroundColor: 'white',
    flexDirection: 'row',
    marginVertical: 20,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 5,
  },
  brand: {
    fontSize: 35,
    color: 'red',
    textAlign: 'center',
  },
  title: {
    fontSize: 18,
    color: 'black',
    textAlign: 'center',
    paddingVertical: 10,
  },
  desc: {
    fontSize: 15,
    color: 'gray',
    textAlign: 'center',
    paddingVertical: 10,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 15,
  },
  contain: {paddingHorizontal: 15, flex: 1},
  heroContain: {flex: 1, alignItems: 'center'},
})
