import axios from 'axios';

const API = axios.create({
  baseURL: 'https://api.themoviedb.org/',
});

const URL = {
  login: 'login',
  movie: '3/trending/movie/day?api_key=7bea80ef513b5f28fbe3846e84b21d58',
};

const IMG = {
  path: 'https://image.tmdb.org/t/p/w500',
};

export {API, URL, IMG};
