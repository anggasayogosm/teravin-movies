import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import SwitchLangReducer from './SwitchLangReducer';
import MoviesReducer from './MovieReducer';

const rootReducer = combineReducers({
  AuthReducer,
  SwitchLangReducer,
  MoviesReducer,
});

export default rootReducer;
