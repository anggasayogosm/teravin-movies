import {Types} from '../Constans';

export const MoviesActions = {
  doGetMovieListRequest: () => ({
    type: Types.GET_MOVIE_LIST_REQUEST,
  }),
  doGetMovieListSuccess: data => ({
    type: Types.GET_MOVIE_LIST_SUCCESS,
    payload: data,
  }),
  doGetMovieListFailure: error => ({
    type: Types.GET_MOVIE_LIST_FAILURE,
    error,
  }),
};

const initialState = {
  movie: {data: [], fetching: false, error: false},
};

export const MoviesSelector = {
  getMovies: state => state.MoviesReducer.language,
};

const MoviesReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_MOVIE_LIST_REQUEST:
      return {
        ...state,
        movie: {data: [], fetching: true, error: null},
      };
    case Types.GET_MOVIE_LIST_SUCCESS:
      return {
        ...state,
        movie: {data: action.payload, fetching: false, error: null},
      };
    case Types.GET_MOVIE_LIST_FAILURE:
      return {
        ...state,
        movie: {data: [], fetching: false, error: action.error},
      };
    default:
      return state;
  }
};

export default MoviesReducer;
