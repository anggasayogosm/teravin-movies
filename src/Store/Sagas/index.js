import {all, fork} from 'redux-saga/effects';
import actionWatchAuth from './AuthSagas';
import actionWatchMovies from './MovieSagas';

export default function* rootSagas() {
  yield all([fork(actionWatchAuth), fork(actionWatchMovies)]);
}
