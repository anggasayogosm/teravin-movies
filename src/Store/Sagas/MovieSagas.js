import {call, put, takeLatest} from 'redux-saga/effects';
import {API, URL} from '../../Services/ApiServices';
import {Types} from '../Constans';
import {MoviesActions} from '../Redux/MovieReducer';

function* doGetMovies() {
  try {
    const response = yield call(API.get, URL.movie);

    yield put(MoviesActions.doGetMovieListSuccess(response.data));
  } catch (error) {
    yield put(MoviesActions.doGetMovieListFailure(error));
  }
}

export default function* actionWatchMovies() {
  yield takeLatest(Types.GET_MOVIE_LIST_REQUEST, doGetMovies);
}
